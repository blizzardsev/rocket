﻿//On load
$(document).ready(function () {

    //Expand content panels gracefully
    $('.site_content').fadeIn(600);

    //Handle selected tab
    var tab = parseInt($('#selectTab').attr('tabnumber'));
    $('.site_navtab').eq(tab).addClass('ext');

    //Handle theme
    var theme = parseInt($('#appTheme').attr('themetype'));
    switch (theme) {

        case 1: //Dark
            $('body').addClass('darktheme');
            $('.site_content').addClass('darktheme');
            $('.site_banner').addClass('darktheme');
            $('.site_textheader').addClass('darktheme');
            $('.site_textheader_mini').addClass('darktheme');
            $('.site_textcontainer').addClass('darktheme');
            $('.site_button').addClass('darktheme');
            $('.site_scrollingcontainer').addClass('darktheme');
            $('.site_scrollitem').addClass('darktheme');

            $('.site_modal_container').addClass('darktheme');
            break;
        case 2: //Blue
            $('body').addClass('bluetheme');
            $('.site_content').addClass('bluetheme');
            $('.site_banner').addClass('bluetheme');
            $('.site_textheader').addClass('bluetheme');
            $('.site_textheader_mini').addClass('bluetheme');
            $('.site_textcontainer').addClass('bluetheme');
            $('.site_button').addClass('bluetheme');
            $('.site_scrollingcontainer').addClass('bluetheme');
            $('.site_scrollitem').addClass('bluetheme');

            $('.site_modal_container').addClass('bluetheme');
            break;
    }

    //Handle dynamic images
    $('#inputAvatar').change(function () {
        var previewFile = $('#inputAvatar').val();
        $('#avatarPreview').attr("src", '/images/Avatar/' + previewFile + '.png');
    })

    //Form submissions
    $('#submitForm').click(function () {
        $('form').submit();
    });

    //Hide modal(s)
    $('.modalCancelButton').click(function () {
        $('.site_modal_container').fadeOut(300);
        $('.site_modal_back').fadeOut(300);
    })


    //Random text on clicking navtab username
    $('.site_userbox').click(function () {

        var possibleLines =
        [
            "Hello There!", "Funny Funny!", "[Insert quip here]",
            "Blast off with Rocket!", "Anime is real!", "Buy Dan a drink!",
            "How's it going?", "var 'MEME' not defined.", "No talent!",
            "Mmmm.. CSS..", "Looking good!", "What a story, Mark!",
            "Without really knowing why..", "You alright, mate?", "Like Sloth from the Goonies!",
            "Hey, good lookin'!", "This statement is false!", "You could be whatever you want!",
            "Gotta catch 'em all!", "Don't deal with the Devil!", "We're on a spaceship, Arthur!"
            ];

        var randGen = Math.floor((Math.random() * possibleLines.length) + 1);
        $('#greetingText').text(possibleLines[randGen]).fadeIn(250);
    });

    //Reset text on leaving navtab username
    $('.site_userbox').mouseleave(function () {
        setTimeout(function () {
            if (!$('.site_userbox').eq(0).is(':hover')) {
                $('#greetingText').text($('#greetingText').attr('heldTime')).fadeIn(500);
            }
        }, 500)
    });

});
