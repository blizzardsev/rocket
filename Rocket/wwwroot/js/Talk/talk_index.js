﻿//On load
$(document).ready(function () {

    //Show topic creation dialogue
    $('#talkCreate_Topic').click(function () {
        $('.site_modal_back').fadeIn(300);
        $('.site_modal_container').fadeIn(300);
    });

    //Show post creation dialogue
    $('#talkCreate_Post').click(function () {
        $('.site_modal_back').fadeIn(300);
        $('#modalPost').fadeIn(300);
    });

    //Show account profile dialogue
    $('.site_scrollitem.profile').click(function () {

        //Assign profile modal attributes from this
        $('#modalProfile_Title').text($(this).attr('postAccountName') + "'s Profile");
        $('#modalProfile_Avatar').attr('src', '/images/Avatar/' + $(this).attr('postAccountAvatar') + '.png');
        $('#modalProfile_Bio_Text').text($(this).attr('postAccountBio'));
        $('#modalProfile_Online_Text').text($(this).attr('postAccountLastOn').toString('d'));
        $('#modalProfile_Activity_Text').text($(this).attr('postAccountNumPosts').toString() + ' comments!');

        $('.site_modal_back').fadeIn(300);
        $('#modalProfile').fadeIn(300);
    });

    //Submit topic creation form
    $('#modalConfirm_Topic').click(function () {
        $('form').submit();
    });

    //Submit topic creation form
    $('#modalConfirm_Post').click(function () {
        $('form').submit();
    });
});