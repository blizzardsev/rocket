﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Rocket.Models;
using Rocket.Data;

namespace Rocket.Controllers
{
    public class ManageController : Controller
    {
        private readonly RocketContext _context;

        //Constructor
        public ManageController(RocketContext context)
        {
            _context = context;
        }

        //Manage Home
        public IActionResult Index()
        {
            //Attempt to login, create account if not existing
            String mySystemName = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            String myUserName = mySystemName.Split('\\')[1];
            Account myAccount = _context.Account.Where(i => i.accountName == myUserName).Single();

            //Get accounts, get topics
            List<Account> allAccounts = _context.Account.ToList();
            List<Topic> allTopics = _context.Topic.ToList();

            //Populate topic fields
            foreach (Topic thisTopic in allTopics)
            {
                thisTopic.topicAccount = _context.Account.Where(i => i.accountID == thisTopic.topicAccountID).Single();
            }

            ViewData["User"] = myAccount;
            ViewData["Username"] = myAccount.accountName;
            ViewData["UserAvatar"] = myAccount.accountAvatar + ".png";
            ViewData["SelectedTab"] = 4;

            ViewData["AllAccounts"] = allAccounts;
            ViewData["AllTopics"] = allTopics;

            return View();
        }
    }
}