﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Rocket.Data;
using Rocket.Models;

namespace Rocket.Controllers
{
    public class SettingsController : Controller
    {
        private readonly RocketContext _context;

        //Constructor
        public SettingsController(RocketContext context)
        {
            _context = context;
        }

        //Settings Home
        public IActionResult Index()
        {
            //Attempt to login, get details
            String mySystemName = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            String myUserName = mySystemName.Split('\\')[1];
            Account  myAccount = _context.Account.Where(i => i.accountName == myUserName).Single();

            ViewData["User"] = myAccount;
            ViewData["Username"] = myAccount.accountName;
            ViewData["UserAvatar"] = myAccount.accountAvatar + ".png";
            ViewData["SelectedTab"] = 1;

            return View();
        }

        //Update user settings
        public async Task<IActionResult> UpdateUserSettings(String inputTheme, string inputTab, String inputAvatar, String inputBio = "Hello there!")
        {
            //Get user
            String mySystemName = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            String myUserName = mySystemName.Split('\\')[1];
            Account myAccount = _context.Account.Where(i => i.accountName == myUserName).Single();

            //Update account
            myAccount.accountTheme = inputTheme;
            myAccount.accountTabs = inputTab;
            myAccount.accountAvatar = inputAvatar;
            myAccount.accountBio = inputBio;

            //Save and return
            _context.Update(myAccount);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index", "Settings");
        }
    }
}