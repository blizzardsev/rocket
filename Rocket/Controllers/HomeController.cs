﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Rocket.Models;
using Rocket.Data;

namespace Rocket.Controllers
{
    public class HomeController : Controller
    {
        private readonly RocketContext _context;

        //Constructor
        public HomeController(RocketContext context)
        {
            _context = context;
        }

        //Rocket App Home
        public IActionResult Index()
        {
            //Attempt to login, create account if not existing
            String mySystemName = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            String myUserName = mySystemName.Split('\\')[1];
            Account myAccount;

            //Attempt to get existing account credentials
            try
            {
                myAccount = _context.Account.Where(i => i.accountName == myUserName).Single();
                myAccount.accountLastLogin = DateTime.Now;
                _context.Update(myAccount);
                _context.SaveChanges();
            }
            //User is new, create new account, get credentials
            catch
            {
                //Create the account
                _context.Add(new Account{
                    accountName = myUserName,
                    accountBio = "Hi everyone! My name is " + myUserName + "!",
                    accountTheme = "Blue",
                    accountAvatar = "Avatar_Rocket",
                    accountCreate = DateTime.Now
                    });
                //Save the changes to the database; set the current account instance to the new account
                _context.SaveChanges();
                myAccount = _context.Account.Where(i => i.accountName == mySystemName).Single();
            }

            //Pass extra data to the page
            ViewData["User"] = myAccount;
            ViewData["Username"] = myAccount.accountName;
            ViewData["UserAvatar"] = myAccount.accountAvatar + ".png";
            ViewData["SelectedTab"] = 0;

            //Return the page
            return View();
        }
    }
}
