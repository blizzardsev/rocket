﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Rocket.Models;
using Rocket.Data;

namespace Rocket.Controllers
{
    public class TalkController : Controller
    {
        private readonly RocketContext _context;

        //Constructor
        public TalkController(RocketContext context)
        {
            _context = context;
        }

        //Talk Home
        public IActionResult Index()
        {
            //Attempt to login, create account if not existing
            String mySystemName = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            String myUserName = mySystemName.Split('\\')[1];
            Account myAccount = _context.Account.Where(i => i.accountName == myUserName).Single();

            //Get topics
            List<Topic> myTopics = _context.Topic.ToList();
            foreach (Topic thisTopic in myTopics)
            {
                thisTopic.topicAccount = _context.Account.Where(i => i.accountID == thisTopic.topicAccountID).Single();
            }

            ViewData["User"] = myAccount;
            ViewData["Username"] = myAccount.accountName;
            ViewData["UserAvatar"] = myAccount.accountAvatar + ".png";
            ViewData["SelectedTab"] = 3;

            ViewData["Topics"] = myTopics;

            return View();
        }

        public IActionResult Topic(int topicID)
        {
            //Attempt to login, create account if not existing
            String mySystemName = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            String myUserName = mySystemName.Split('\\')[1];
            Account myAccount = _context.Account.Where(i => i.accountName == myUserName).Single();

            //Get topic, populate topic user
            Topic myTopic = _context.Topic.Where(i => i.topicID == topicID).Single();
            myTopic.topicAccount = _context.Account.Where(i => i.accountID == myTopic.topicAccountID).Single();

            //Get topic posts, populate post topic and post users
            List<Post> myPosts = _context.Post.Where(i => i.postTopicID == myTopic.topicID).ToList();
            foreach (Post thisPost in myPosts)
            {
                thisPost.postAccount = _context.Account.Where(i => i.accountID == thisPost.postAccountID).Single();
                thisPost.postAccount.accountPosts = _context.Post.Where(i => i.postAccountID == thisPost.postAccountID).ToList();
                thisPost.postTopic = myTopic;
            }

            ViewData["User"] = myAccount;
            ViewData["Username"] = myAccount.accountName;
            ViewData["UserAvatar"] = myAccount.accountAvatar + ".png";
            ViewData["SelectedTab"] = 3;

            ViewData["Topic"] = myTopic;
            ViewData["Posts"] = myPosts;

            return View();
        }

        //Create new topic
        public async Task<IActionResult> CreateTopic(String inputTitle, String inputDesc)
        {
            //Get user
            String mySystemName = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            String myUserName = mySystemName.Split('\\')[1];
            Account myAccount = _context.Account.Where(i => i.accountName == myUserName).Single();

            //Create topic, save and return
            _context.Add(new Topic
            {
                topicTitle = inputTitle,
                topicDesc = inputDesc,
                topicAccountID = myAccount.accountID,
                topicActive = true,
            });
            await _context.SaveChangesAsync();
            return RedirectToAction("Index", "Talk");
        }

        //Create new post 
        public async Task<IActionResult> CreatePost(String inputContent, int inputTopicID)
        {
            //Get user
            String mySystemName = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            String myUserName = mySystemName.Split('\\')[1];
            Account myAccount = _context.Account.Where(i => i.accountName == myUserName).Single();

            //Create post, save and return
            _context.Add(new Post
            {
                postContent = inputContent,
                postDate = DateTime.Now,
                postAccountID = myAccount.accountID,
                postTopicID = _context.Topic.Where(i => i.topicID == inputTopicID).Single().topicID,
            });
            await _context.SaveChangesAsync();
            return RedirectToAction("Topic", "Talk", new { topicID = inputTopicID });
        }

    }
}