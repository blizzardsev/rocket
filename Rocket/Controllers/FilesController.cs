﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Rocket.Models;
using Rocket.Data;

namespace Rocket.Controllers
{
    public class FilesController : Controller
    {
        private readonly RocketContext _context;

        //Constructor
        public FilesController(RocketContext context)
        {
            _context = context;
        }

        //Files Home
        public IActionResult Index()
        {
            //Attempt to login, create account if not existing
            String mySystemName = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            String myUserName = mySystemName.Split('\\')[1];
            Account myAccount = _context.Account.Where(i => i.accountName == myUserName).Single();

            ViewData["User"] = myAccount;
            ViewData["Username"] = myAccount.accountName;
            ViewData["UserAvatar"] = myAccount.accountAvatar + ".png";
            ViewData["SelectedTab"] = 2;

            return View();
        }
    }
}