﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Rocket.Models;

namespace Rocket.Data
{
    public class RocketContext : DbContext
    {
        public RocketContext(DbContextOptions<RocketContext> options) : base(options)
        {

        }

        public DbSet<Account> Account { get; set; }
        public DbSet<Topic> Topic { get; set; }
        public DbSet<Post> Post { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Account>().ToTable("Account");
            modelBuilder.Entity<Topic>().ToTable("Topic");
            modelBuilder.Entity<Post>().ToTable("Post");
        }
    }
}
