﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rocket.Models
{
    public class Account
    {
        //Account specifics
        public int accountID { get; set; }
        public string accountName { get; set; }
        public string accountBio { get; set; }
        public string accountAvatar { get; set; }

        //Account statistics
        public DateTime accountCreate { get; set; }
        public DateTime accountLastLogin { get; set; }

        //Settings
        public string accountTheme { get; set; }
        public string accountTabs { get; set; }

        //Page-specific
        public List<Post> accountPosts;
    }
}
