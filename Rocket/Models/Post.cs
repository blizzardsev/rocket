﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rocket.Models
{
    public class Post
    {
        //Topic specifics
        public int postID { get; set; }
        public string postContent { get; set; }
        public DateTime postDate { get; set; }
        public int postAccountID { get; set; }
        public int postTopicID { get; set; }

        //Topic deduced
        public Account postAccount;
        public Topic postTopic;
    }
}
