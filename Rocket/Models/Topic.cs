﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rocket.Models
{
    public class Topic
    {
        //Topic specifics
        public int topicID { get; set; }
        public string topicTitle { get; set; }
        public string topicDesc { get; set; }
        public bool topicActive { get; set; }
        public int topicAccountID { get; set; }

        //Topic deduced
        public Account topicAccount;
    }
}
